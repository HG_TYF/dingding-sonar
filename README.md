# dingding-sonar

钉钉的sonar集成通知

## 项目部署
```
#下载release包 /releases/dingding-sonar-1.0-SNAPSHOT.jar
wget https://gitee.com/chejiangyi/dingding-sonar/repository/archive/master.zip
unzip master.zip

#运行jar包 sonar.url为sonar安装地址
cd dingding-sonar/releases
nohup java -jar dingding-sonar-1.0-SNAPSHOT.jar \
--server.port=8082 \
--sonar.url=http://10.252.193.11:9000 \
> /dev/null 2 >& 1 &

#检查部署成功?? sonar-project-id 为sonar的项目id example:com.yh.csx.crm:csx-b2b-crm
curl curl http://localhost:8082/?projectname-{sonar-project-id}
###返回:bug:11,漏洞:11,坏味道:456,覆盖率:0.0,重复:18.7,状态:ERROR

```

## 钉钉集成
```
#!/bin/bash
#参考钉钉文档 https://open-doc.dingtalk.com/microapp/serverapi2/qf2nxq
 sonarreport=$(curl -s http://localhost:8082/?projectname={sonar-project-id})
 curl -s "https://oapi.dingtalk.com/robot/send?access_token=${dingding_token}" \
   -H "Content-Type: application/json" \
   -d "{
     \"msgtype\": \"markdown\",
     \"markdown\": {
         \"title\":\"sonar代码质量\",
         \"text\": \"## sonar代码质量报告: \n
> [sonar地址](http://10.252.193.11:9000/dashboard?id={sonar-project-id}) \n
> ${sonarreport} \n\"
     }
 }"
```

##### by 车江毅